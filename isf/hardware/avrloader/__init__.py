import os, platform, glob, zipfile, requests, tarfile, subprocess, json
from .options import links
from isf.core import logger
from subprocess import check_output, STDOUT


class AVRLoader:
    platform_os = ''
    executable = ''
    avrloader_path = ''

    def __init__(self):
        self.avrloader_path = os.path.dirname(os.path.abspath(__file__))
        self.platform_os = platform.system()
        self.executable = self.check_deps()
        if self.executable == '':
            self.download_deps()
        self.executable = self.check_deps()
        if self.executable == '':
            logger.error("Can't download executable! Check this,please")
        logger.info('Using executable: {}'.format(self.executable))

    def __del__(self):
        pass

    def check_deps(self):
        # TODO: add support for ARM, ARM64 and linux64
        if self.platform_os == 'Windows':
            files = glob.glob(
                self.avrloader_path + '/arduino-cli/arduino-cli*.exe')
            if len(files) == 0:
                return ''
            else:
                return files[0]
        elif self.platform_os == 'Linux':
            files = glob.glob(
                self.avrloader_path + '/arduino-cli/arduino-cli*-linux32')
            if len(files) == 0:
                return ''
            else:
                return files[0]
        elif self.platform_os == 'Darwin':
            files = glob.glob(
                self.avrloader_path + '/arduino-cli/arduino-cli*-macos')
            if len(files):
                return ''
            else:
                return files[0]

    def download_deps(self):
        # TODO: add support for ARM, ARM64 and linux64
        zip_path = self.avrloader_path + '/arduino-cli/arduino-cli.zip'
        tar_path = self.avrloader_path + '/arduino-cli/arduino-cli.tar.bz2'
        unpack_path = self.avrloader_path + '/arduino-cli/'

        #todo: move all to resources folder
        if platform.system() == 'Windows':
            r = requests.get(links['windows'], allow_redirects=True)
            open(zip_path, 'wb').write(r.content)
            zip_ref = zipfile.ZipFile(zip_path, 'r')
            zip_ref.extractall(unpack_path)
            zip_ref.close()
        elif platform.system() == 'Linux':
            r = requests.get(links['linux32'], allow_redirects=True)
            open(tar_path, 'wb').write(r.content)
            tar = tarfile.open(tar_path, "r:bz2")
            tar.extractall(unpack_path)
            tar.close()
            # TODO: need testing

        elif platform.system() == 'Darwin':
            r = requests.get(links['macos'], allow_redirects=True)
            open(zip_path, 'wb').write(r.content)
            zip_ref = zipfile.ZipFile(zip_path, 'r')
            zip_ref.extractall(unpack_path)
            zip_ref.close()

    def code_compiler(self, source_path, dest_path, timeout=10):
        # TODO: add timeout popen
        if self.executable == '':
            return {"status": -1}
        logger.info("Installing arduino:avr support")
        args = (self.executable, "core", "install", "arduino:avr")
        popen = subprocess.Popen(args, stdout=subprocess.PIPE)
        popen.wait()
        output = popen.stdout.read()
        logger.debug(output.decode())
        logger.info("Compiling {} to {}".format(source_path, dest_path))
        args = (
            self.executable, "compile", "--fqbn", "arduino:avr:uno",
            source_path,
            "-o", dest_path)
        popen = subprocess.Popen(args, stdout=subprocess.PIPE)
        popen.wait()
        output = popen.stdout.read()
        logger.debug(output.decode())
        return {"status": popen.returncode}

    def hex_uploader(self, hex_path, com_port, timeout=10):
        # TODO: add timeout popen
        if self.executable == '':
            return {"status": -1}
        logger.info("Uploading code to {}".format(com_port))
        args = (
            self.executable, "upload", "-p", com_port, "--fqbn",
            "arduino:avr:uno", "-i", hex_path)
        popen = subprocess.Popen(args, stdout=subprocess.PIPE)
        popen.wait()
        output = popen.stdout.read()
        logger.debug(output.decode())
        return {"status": popen.returncode}

    def lib_installer(self, lib_name, timeout):
        # TODO: add timeout popen
        if self.executable == '':
            return {"status": -1}
        logger.info("Installing lib {} to executable {}".format(lib_name,
                                                                self.executable))
        args = (self.executable, "lib", "install", lib_name)
        popen = subprocess.Popen(args, stdout=subprocess.PIPE)
        popen.wait()
        output = popen.stdout.read()
        logger.debug(output.decode())
        return {"status": popen.returncode}

    def lib_finder(self, search_name, timeout):
        if self.executable == '':
            return {"status": -1}
        logger.info("Finging lib \"{}\" with executable {}".format(search_name,
                                                                   self.executable))
        args = (
            self.executable, "lib", "search", search_name, "--format", "json")
        output = check_output(args, timeout=timeout)
        logger.debug(output.decode())
        j = json.loads(output.decode())
        # TODO: error with output
        return j

    def ino_uploader(self, ino_path, com_port, tmp_hex='/tmp/isf.hex', timeout=10):
        if self.executable == '':
            return {"status": -1}
        res = self.code_compiler(ino_path, tmp_hex, timeout)
        if res["status"] != 0:
            return res
        res = self.hex_uploader(tmp_hex, com_port, timeout)
        return res
