
links = {
    "windows": "https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-windows.zip",
    "linux32": "https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linux32.tar.bz2",
    "linux64": "https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linux64.tar.bz2",
    "macos": "https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-osx.zip",
    "arm32": "https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linuxarm.tar.bz2",
    "arm64": "https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linuxaarch64.tar.bz2"
}